import _root_.sbt.Keys._

name := "NationBuilderChallenge"

version := "1.0"

libraryDependencies ++= Seq(
  "joda-time"     % "joda-time" % "2.3"
)

lazy val root = (project in file(".")).enablePlugins(PlayScala)
