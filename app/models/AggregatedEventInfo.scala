package models

import scala.collection.mutable.{Set => MutableSet}

trait AggregatedEventInfo {
  def hasAnyEvents: Boolean
  def eventDescription: String
  def addEvent(event: ChatEvent): Unit
}

class EnterRoomAggregatedEventInfo() extends AggregatedEventInfo {
  val people = MutableSet[String]()

  def hasAnyEvents = people.size > 0

  def eventDescription = {
    if (people.size == 1) s"${people.size} person entered"
    else s"${people.size} people entered"
  }

  def addEvent(event: ChatEvent) = {
    people += event.userName
  }
}

class LeaveRoomAggregatedEventInfo() extends AggregatedEventInfo {
  val people = MutableSet[String]()

  def hasAnyEvents = people.size > 0

  def eventDescription = {
    if (people.size == 1) s"${people.size} person left"
    else s"${people.size} people left"
  }

  def addEvent(event: ChatEvent) = {
    people += event.userName
  }
}

class CommentAggregatedEventInfo() extends AggregatedEventInfo {
  var commentCount: Int = 0

  def hasAnyEvents = commentCount > 0

  def eventDescription = {
    if (commentCount == 1) s"${commentCount} comment"
    else s"${commentCount} comments"
  }

  def addEvent(event: ChatEvent) = {
    commentCount += 1
  }
}

class HighFiveAggregatedEventInfo() extends AggregatedEventInfo {
  val highFivers = MutableSet[String]()
  val highFiveRecipients = MutableSet[String]()

  def hasAnyEvents = highFivers.size > 0 && highFiveRecipients.size > 0

  def eventDescription = {
    val highFiverTerm = if (highFivers.size == 1) "person" else "people"
    val recipientTerm = if (highFiveRecipients.size == 1) "person" else "people"

    s"${highFivers.size} ${highFiverTerm} high-fived ${highFiveRecipients.size} other $recipientTerm"
  }

  def addEvent(event: ChatEvent) = {
    event match {
      case HighFiveUserEvent(highFiver: String, _, highFiveRecipient: String) => {
        highFivers += highFiver
        highFiveRecipients += highFiveRecipient
      }
    }
  }
}
