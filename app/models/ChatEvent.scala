package models

import org.joda.time.DateTime

object ChatEventType extends Enumeration {
  type ChatEventType = Value
  val EnterRoom = Value
  val LeaveRoom = Value
  val Comment = Value
  val HighFive = Value
}

import ChatEventType.ChatEventType
trait ChatEvent {
  val userName: String
  val timestamp: DateTime
  val eventType: ChatEventType
  def eventDescription: String
}

case class EnterRoomEvent(userName: String, timestamp: DateTime) extends ChatEvent {
  val eventType = ChatEventType.EnterRoom
  def eventDescription = s"$userName entered the room"
}

case class LeaveRoomEvent(userName: String, timestamp: DateTime) extends ChatEvent {
  val eventType = ChatEventType.LeaveRoom
  def eventDescription = s"$userName left the room"
}

case class CommentEvent(userName: String, timestamp: DateTime, commentText: String) extends ChatEvent {
  val eventType = ChatEventType.Comment
  def eventDescription = s"$userName comments: '$commentText'"
}

case class HighFiveUserEvent(userName: String, timestamp: DateTime, highFiveTargetName: String) extends ChatEvent {
  val eventType = ChatEventType.HighFive
  def eventDescription = s"$userName high-fives $highFiveTargetName"
}

