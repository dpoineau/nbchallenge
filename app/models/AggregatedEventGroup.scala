package models

import ChatEventType.ChatEventType
import models._

import scala.collection.mutable.LinkedHashMap

case class AggregatedEventGroup(val groupTimeText: String) {
  // Note that I use LinkedHashMap because map will iterate over it in insertion order
  // which is convenient for printing the summary info in this defined order
  val aggregatedEvents = LinkedHashMap(
    ChatEventType.EnterRoom -> new EnterRoomAggregatedEventInfo(),
    ChatEventType.Comment -> new CommentAggregatedEventInfo(),
    ChatEventType.HighFive -> new HighFiveAggregatedEventInfo(),
    ChatEventType.LeaveRoom -> new LeaveRoomAggregatedEventInfo()
  )

  def hasAnyEventOfType(chatEventType: ChatEventType): Boolean = {
    aggregatedEvents.get(chatEventType).map { aggEventInfo =>
      aggEventInfo.hasAnyEvents
    }.getOrElse(false)
  }
}


