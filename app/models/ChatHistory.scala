package models

import models._
import org.joda.time.DateTime

object ChatHistory {
  private val now = DateTime.now()

  implicit def dateTimeOrdering: Ordering[DateTime] = Ordering.fromLessThan(_ isBefore _)

  val allEvents: Seq[ChatEvent] = List(
    EnterRoomEvent("Eve", DateTime.parse("2014-08-01T10:00")),
    CommentEvent("Eve", DateTime.parse("2014-08-01T10:01"), "Hi"),
    LeaveRoomEvent("Eve", DateTime.parse("2014-08-14T23:45")),

    EnterRoomEvent("Bob", DateTime.parse("2014-08-10T03:00")),
    CommentEvent("Bob", DateTime.parse("2014-08-10T10:00"), "Hello!"),
    LeaveRoomEvent("Bob", DateTime.parse("2014-08-14T23:00")),

    EnterRoomEvent("Alice", DateTime.parse("2014-08-03T06:00")),
    CommentEvent("Alice", DateTime.parse("2014-08-03T07:00"), "My comment"),
    CommentEvent("Alice", DateTime.parse("2014-08-03T08:30"), "My other comment"),
    HighFiveUserEvent("Alice", DateTime.parse("2014-08-03T11:00"), "Eve"),
    LeaveRoomEvent("Alice", DateTime.parse("2014-08-03T16:00")),

    EnterRoomEvent("Alice", DateTime.parse("2014-08-14T01:00")),
    HighFiveUserEvent("Alice", DateTime.parse("2014-08-14T02:00"), "Bob"),
    HighFiveUserEvent("Alice", DateTime.parse("2014-08-14T11:00"), "Eve"),
    LeaveRoomEvent("Alice", DateTime.parse("2014-08-14T21:00"))

  ).sortBy(_.timestamp)

  def aggregatedEventsByHour(): Seq[AggregatedEventGroup] = {
    new HourEventAggregator(allEvents).aggregate()
  }

  def aggregatedEventsByDay(): Seq[AggregatedEventGroup] = {
    new DayEventAggregator(allEvents).aggregate()
  }
}
