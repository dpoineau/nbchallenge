package models

import scala.collection.mutable.ArrayBuffer

// Note this class assumes the events passed in are in ascending order by timestamp
abstract class EventAggregator(individualEvents: Seq[ChatEvent]) {

  // Returns an appropriate group identifier for the desired aggregation granularity
  // For example, for hour granularity this would be "yyyy-MM-dd hha", e.g. "2012-01-01 10am"
  protected def getGroupTimeForEvent(event: ChatEvent): String

  private val aggregatedEventGroups = new ArrayBuffer[AggregatedEventGroup]()

  private def processEvent(event: ChatEvent) = {
    // If event is not for same time as last event group, add a new event group for this event time
    val eventGroupTime = getGroupTimeForEvent(event)
    if (aggregatedEventGroups.last.groupTimeText != eventGroupTime) {
      aggregatedEventGroups += AggregatedEventGroup(eventGroupTime)
    }

    val eventGroup = aggregatedEventGroups.last
    // Update the appropriate summary statistics for this group
    eventGroup.aggregatedEvents.get(event.eventType).map { aggregatedEventInfo =>
      aggregatedEventInfo.addEvent(event)
    }
  }

  def aggregate(): Seq[AggregatedEventGroup] = {
    if (individualEvents.size > 0) {
      aggregatedEventGroups.clear()
      aggregatedEventGroups += AggregatedEventGroup(getGroupTimeForEvent(individualEvents.head))

      individualEvents.map { event =>
        processEvent(event)
      }

      aggregatedEventGroups.toSeq
    } else {
      Seq()
    }
  }
}

class HourEventAggregator(individualEvents: Seq[ChatEvent]) extends EventAggregator(individualEvents) {

  protected def getGroupTimeForEvent(event: ChatEvent) = {
    event.timestamp.toString("yyyy-MM-dd hha")
  }
}

class DayEventAggregator(individualEvents: Seq[ChatEvent]) extends EventAggregator(individualEvents) {

  protected def getGroupTimeForEvent(event: ChatEvent) = {
    event.timestamp.toString("yyyy-MM-dd")
  }
}
