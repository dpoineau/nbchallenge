package controllers

import org.joda.time.DateTime
import play.api.mvc.{Action, Controller}
import models.{EnterRoomEvent, ChatHistory}

object MainController extends Controller {

  def allEvents() = Action { implicit request =>

    Ok(views.html.allEventsRenderer(ChatHistory.allEvents))
  }

  def byHour() = Action { implicit request =>
    Ok(views.html.aggregatedRenderer(ChatHistory.aggregatedEventsByHour()))
  }

  def byDay() = Action { implicit request =>
    Ok(views.html.aggregatedRenderer(ChatHistory.aggregatedEventsByDay()))
  }
}
