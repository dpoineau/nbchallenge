package models

import org.joda.time.DateTime
import org.specs2.mutable.Specification

class HourEventAggregatorSpec extends Specification {

  "HourEventAggregator" should {
    "aggregate correctly with no events" in {
      val noEvents = List[ChatEvent]()

      val aggResult = new HourEventAggregator(noEvents).aggregate()

      aggResult.length must beEqualTo(0)
    }

    "aggregate correctly with one event" in {

      val oneEvent = Seq(EnterRoomEvent("Test", DateTime.parse("2014-05-01T13:00")))
      val aggResult = new HourEventAggregator(oneEvent).aggregate()

      aggResult.length must beEqualTo(1)
      aggResult.head.groupTimeText must beEqualTo("2014-05-01 01PM")

      val enterRoomAggedEventInfo = aggResult.head.aggregatedEvents.get(ChatEventType.EnterRoom).get
      enterRoomAggedEventInfo.hasAnyEvents must beTrue
    }

    "aggregate correctly with multiple events of same type within 1 hour" in {

      val multipleEvents = Seq(
        EnterRoomEvent("Test", DateTime.parse("2014-05-01T13:00")),
        EnterRoomEvent("Test2", DateTime.parse("2014-05-01T13:05")),
        EnterRoomEvent("Test3", DateTime.parse("2014-05-01T13:40"))
      )

      val aggResult = new HourEventAggregator(multipleEvents).aggregate()

      aggResult.length must beEqualTo(1)
      aggResult.head.groupTimeText must beEqualTo("2014-05-01 01PM")

      aggResult.head.hasAnyEventOfType(ChatEventType.EnterRoom) must beTrue
      aggResult.head.hasAnyEventOfType(ChatEventType.LeaveRoom) must beFalse
      aggResult.head.hasAnyEventOfType(ChatEventType.Comment) must beFalse
      aggResult.head.hasAnyEventOfType(ChatEventType.HighFive) must beFalse
    }

    "aggregate correctly with multiple events of different types within 1 hour" in {
      val multipleEvents = Seq(
        EnterRoomEvent("Test", DateTime.parse("2014-05-01T13:00")),
        LeaveRoomEvent("Test2", DateTime.parse("2014-05-01T13:05")),
        CommentEvent("Test3", DateTime.parse("2014-05-01T13:10"), "Test comment"),
        HighFiveUserEvent("Test3", DateTime.parse("2014-05-01T13:25"), "Test2")
      )

      val aggResult = new HourEventAggregator(multipleEvents).aggregate()

      aggResult.length must beEqualTo(1)
      aggResult.head.groupTimeText must beEqualTo("2014-05-01 01PM")

      aggResult.head.hasAnyEventOfType(ChatEventType.EnterRoom) must beTrue
      aggResult.head.hasAnyEventOfType(ChatEventType.LeaveRoom) must beTrue
      aggResult.head.hasAnyEventOfType(ChatEventType.Comment) must beTrue
      aggResult.head.hasAnyEventOfType(ChatEventType.HighFive) must beTrue
    }

    "aggregate correctly with events spread across two consecutive hours" in {
      val multipleEvents = Seq(
        EnterRoomEvent("Test", DateTime.parse("2014-05-01T01:10")),
        LeaveRoomEvent("Test2", DateTime.parse("2014-05-01T01:20")),
        CommentEvent("Test3", DateTime.parse("2014-05-01T02:09"), "Test comment"),
        HighFiveUserEvent("Test3", DateTime.parse("2014-05-01T02:10"), "Test2")
      )

      val aggResult = new HourEventAggregator(multipleEvents).aggregate()

      aggResult.length must beEqualTo(2)

      val firstHour: AggregatedEventGroup = aggResult(0)
      val secondHour: AggregatedEventGroup = aggResult(1)

      firstHour.groupTimeText must beEqualTo("2014-05-01 01AM")
      secondHour.groupTimeText must beEqualTo("2014-05-01 02AM")

      firstHour.aggregatedEvents.get(ChatEventType.EnterRoom).get.hasAnyEvents must beTrue
      firstHour.aggregatedEvents.get(ChatEventType.LeaveRoom).get.hasAnyEvents must beTrue
      firstHour.aggregatedEvents.get(ChatEventType.Comment).get.hasAnyEvents must beFalse
      firstHour.aggregatedEvents.get(ChatEventType.HighFive).get.hasAnyEvents must beFalse

      secondHour.aggregatedEvents.get(ChatEventType.EnterRoom).get.hasAnyEvents must beFalse
      secondHour.aggregatedEvents.get(ChatEventType.LeaveRoom).get.hasAnyEvents must beFalse
      secondHour.aggregatedEvents.get(ChatEventType.Comment).get.hasAnyEvents must beTrue
      secondHour.aggregatedEvents.get(ChatEventType.HighFive).get.hasAnyEvents must beTrue
    }

    "aggregate correctly with events spread across two non-consecutive hours" in {
      val multipleEvents = Seq(
        EnterRoomEvent("Test", DateTime.parse("2014-05-01T01:10")),
        LeaveRoomEvent("Test2", DateTime.parse("2014-05-01T01:20")),
        CommentEvent("Test3", DateTime.parse("2014-05-01T04:09"), "Test comment"),
        HighFiveUserEvent("Test3", DateTime.parse("2014-05-01T04:10"), "Test2")
      )

      val aggResult = new HourEventAggregator(multipleEvents).aggregate()

      aggResult.length must beEqualTo(2)

      val firstHour: AggregatedEventGroup = aggResult(0)
      val secondHour: AggregatedEventGroup = aggResult(1)

      firstHour.groupTimeText must beEqualTo("2014-05-01 01AM")
      secondHour.groupTimeText must beEqualTo("2014-05-01 04AM")

      firstHour.aggregatedEvents.get(ChatEventType.EnterRoom).get.hasAnyEvents must beTrue
      firstHour.aggregatedEvents.get(ChatEventType.LeaveRoom).get.hasAnyEvents must beTrue
      firstHour.aggregatedEvents.get(ChatEventType.Comment).get.hasAnyEvents must beFalse
      firstHour.aggregatedEvents.get(ChatEventType.HighFive).get.hasAnyEvents must beFalse

      secondHour.aggregatedEvents.get(ChatEventType.EnterRoom).get.hasAnyEvents must beFalse
      secondHour.aggregatedEvents.get(ChatEventType.LeaveRoom).get.hasAnyEvents must beFalse
      secondHour.aggregatedEvents.get(ChatEventType.Comment).get.hasAnyEvents must beTrue
      secondHour.aggregatedEvents.get(ChatEventType.HighFive).get.hasAnyEvents must beTrue
    }
  }
}
