package models

import org.joda.time.DateTime
import org.specs2.mutable.Specification

class DayEventAggregatorSpec extends Specification {

  "DayEventAggregator" should {
    "aggregate correctly with no events" in {
      val noEvents = List[ChatEvent]()

      val aggResult = new DayEventAggregator(noEvents).aggregate()

      aggResult.length must beEqualTo(0)
    }

    "aggregate correctly with one event" in {

      val oneEvent = Seq(EnterRoomEvent("Test", DateTime.parse("2014-05-01T13:00")))
      val aggResult = new DayEventAggregator(oneEvent).aggregate()

      aggResult.length must beEqualTo(1)
      aggResult.head.groupTimeText must beEqualTo("2014-05-01")

      val enterRoomAggedEventInfo = aggResult.head.aggregatedEvents.get(ChatEventType.EnterRoom).get
      enterRoomAggedEventInfo.hasAnyEvents must beTrue
    }

    "aggregate correctly with multiple events of same type within 1 day" in {

      val multipleEvents = Seq(
        EnterRoomEvent("Test", DateTime.parse("2014-05-01T13:00")),
        EnterRoomEvent("Test2", DateTime.parse("2014-05-01T14:00")),
        EnterRoomEvent("Test3", DateTime.parse("2014-05-01T15:00"))
      )

      val aggResult = new DayEventAggregator(multipleEvents).aggregate()

      aggResult.length must beEqualTo(1)
      aggResult.head.groupTimeText must beEqualTo("2014-05-01")

      aggResult.head.hasAnyEventOfType(ChatEventType.EnterRoom) must beTrue
      aggResult.head.hasAnyEventOfType(ChatEventType.LeaveRoom) must beFalse
      aggResult.head.hasAnyEventOfType(ChatEventType.Comment) must beFalse
      aggResult.head.hasAnyEventOfType(ChatEventType.HighFive) must beFalse
    }

    "aggregate correctly with multiple events of different types within 1 day" in {
      val multipleEvents = Seq(
        EnterRoomEvent("Test", DateTime.parse("2014-05-01T13:00")),
        LeaveRoomEvent("Test2", DateTime.parse("2014-05-01T14:00")),
        CommentEvent("Test3", DateTime.parse("2014-05-01T15:00"), "Test comment"),
        HighFiveUserEvent("Test3", DateTime.parse("2014-05-01T15:00"), "Test2")
      )

      val aggResult = new DayEventAggregator(multipleEvents).aggregate()

      aggResult.length must beEqualTo(1)
      aggResult.head.groupTimeText must beEqualTo("2014-05-01")

      aggResult.head.hasAnyEventOfType(ChatEventType.EnterRoom) must beTrue
      aggResult.head.hasAnyEventOfType(ChatEventType.LeaveRoom) must beTrue
      aggResult.head.hasAnyEventOfType(ChatEventType.Comment) must beTrue
      aggResult.head.hasAnyEventOfType(ChatEventType.HighFive) must beTrue
    }

    "aggregate correctly with events spread across two consecutive days" in {
      val multipleEvents = Seq(
        EnterRoomEvent("Test", DateTime.parse("2014-05-01T13:00")),
        LeaveRoomEvent("Test2", DateTime.parse("2014-05-01T14:00")),
        CommentEvent("Test3", DateTime.parse("2014-05-02T15:00"), "Test comment"),
        HighFiveUserEvent("Test3", DateTime.parse("2014-05-02T15:00"), "Test2")
      )

      val aggResult = new DayEventAggregator(multipleEvents).aggregate()

      aggResult.length must beEqualTo(2)

      val firstDay: AggregatedEventGroup = aggResult(0)
      val secondDay: AggregatedEventGroup = aggResult(1)

      firstDay.groupTimeText must beEqualTo("2014-05-01")
      secondDay.groupTimeText must beEqualTo("2014-05-02")

      firstDay.aggregatedEvents.get(ChatEventType.EnterRoom).get.hasAnyEvents must beTrue
      firstDay.aggregatedEvents.get(ChatEventType.LeaveRoom).get.hasAnyEvents must beTrue
      firstDay.aggregatedEvents.get(ChatEventType.Comment).get.hasAnyEvents must beFalse
      firstDay.aggregatedEvents.get(ChatEventType.HighFive).get.hasAnyEvents must beFalse

      secondDay.aggregatedEvents.get(ChatEventType.EnterRoom).get.hasAnyEvents must beFalse
      secondDay.aggregatedEvents.get(ChatEventType.LeaveRoom).get.hasAnyEvents must beFalse
      secondDay.aggregatedEvents.get(ChatEventType.Comment).get.hasAnyEvents must beTrue
      secondDay.aggregatedEvents.get(ChatEventType.HighFive).get.hasAnyEvents must beTrue
    }

    "aggregate correctly with events spread across two non-consecutive days" in {
      val multipleEvents = Seq(
        EnterRoomEvent("Test", DateTime.parse("2014-05-01T13:00")),
        LeaveRoomEvent("Test2", DateTime.parse("2014-05-01T14:00")),
        CommentEvent("Test3", DateTime.parse("2014-05-03T15:00"), "Test comment"),
        HighFiveUserEvent("Test3", DateTime.parse("2014-05-03T15:00"), "Test2")
      )

      val aggResult = new DayEventAggregator(multipleEvents).aggregate()

      aggResult.length must beEqualTo(2)

      val firstDay: AggregatedEventGroup = aggResult(0)
      val secondDay: AggregatedEventGroup = aggResult(1)

      firstDay.groupTimeText must beEqualTo("2014-05-01")
      secondDay.groupTimeText must beEqualTo("2014-05-03")

      firstDay.aggregatedEvents.get(ChatEventType.EnterRoom).get.hasAnyEvents must beTrue
      firstDay.aggregatedEvents.get(ChatEventType.LeaveRoom).get.hasAnyEvents must beTrue
      firstDay.aggregatedEvents.get(ChatEventType.Comment).get.hasAnyEvents must beFalse
      firstDay.aggregatedEvents.get(ChatEventType.HighFive).get.hasAnyEvents must beFalse

      secondDay.aggregatedEvents.get(ChatEventType.EnterRoom).get.hasAnyEvents must beFalse
      secondDay.aggregatedEvents.get(ChatEventType.LeaveRoom).get.hasAnyEvents must beFalse
      secondDay.aggregatedEvents.get(ChatEventType.Comment).get.hasAnyEvents must beTrue
      secondDay.aggregatedEvents.get(ChatEventType.HighFive).get.hasAnyEvents must beTrue
    }
  }
}
