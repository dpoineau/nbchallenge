package models

import org.joda.time.DateTime
import org.specs2.mutable.Specification
import models._

class AggregatedEventInfoSpec extends Specification {
  "CommentAggregatedEventInfo" should {
    "correctly track one comment" in {

      val aggInfo = new CommentAggregatedEventInfo()
      aggInfo.addEvent(CommentEvent("Bob", DateTime.parse("2014-08-01T01:00"), "Comment"))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.commentCount must beEqualTo(1)
    }

    "correctly track multiple comments" in {
      val aggInfo = new CommentAggregatedEventInfo()
      aggInfo.addEvent(CommentEvent("Bob", DateTime.parse("2014-08-01T01:00"), "Comment"))
      aggInfo.addEvent(CommentEvent("Bob", DateTime.parse("2014-08-01T01:05"), "Comment2"))
      aggInfo.addEvent(CommentEvent("Alice", DateTime.parse("2014-08-01T01:06"), "Comment3"))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.commentCount must beEqualTo(3)
    }
  }

  "EnterRoomAggregatedEventInfo" should {
    "correctly track one person entering the room" in {
      val aggInfo = new EnterRoomAggregatedEventInfo()

      aggInfo.addEvent(EnterRoomEvent("Bob", DateTime.parse("2014-08-01T01:00")))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.people.size must beEqualTo(1)
    }

    "correctly track multiple non-repeating people entering the room" in {
      val aggInfo = new EnterRoomAggregatedEventInfo()

      aggInfo.addEvent(EnterRoomEvent("Bob", DateTime.parse("2014-08-01T01:00")))
      aggInfo.addEvent(EnterRoomEvent("Alice", DateTime.parse("2014-08-01T01:00")))
      aggInfo.addEvent(EnterRoomEvent("Eve", DateTime.parse("2014-08-01T01:00")))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.people.size must beEqualTo(3)
    }

    "correctly track one person entering the room multiple times" in {
      val aggInfo = new EnterRoomAggregatedEventInfo()

      aggInfo.addEvent(EnterRoomEvent("Bob", DateTime.parse("2014-08-01T01:00")))
      aggInfo.addEvent(EnterRoomEvent("Bob", DateTime.parse("2014-08-01T02:00")))
      aggInfo.addEvent(EnterRoomEvent("Bob", DateTime.parse("2014-08-01T03:00")))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.people.size must beEqualTo(1)
    }

    "correctly track multiple people repeatedly entering the room" in {
      val aggInfo = new EnterRoomAggregatedEventInfo()

      aggInfo.addEvent(EnterRoomEvent("Bob", DateTime.parse("2014-08-01T01:00")))
      aggInfo.addEvent(EnterRoomEvent("Bob", DateTime.parse("2014-08-01T02:00")))
      aggInfo.addEvent(EnterRoomEvent("Alice", DateTime.parse("2014-08-01T01:00")))
      aggInfo.addEvent(EnterRoomEvent("Alice", DateTime.parse("2014-08-01T02:00")))
      aggInfo.addEvent(EnterRoomEvent("Eve", DateTime.parse("2014-08-01T01:00")))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.people.size must beEqualTo(3)
    }
  }

  "HighFiveAggregatedEventInfo" should {

    "correctly track one person high fiving one other" in {
      val aggInfo = new HighFiveAggregatedEventInfo()

      aggInfo.addEvent(HighFiveUserEvent("Bob", DateTime.parse("2014-08-01T01:00"), "Alice"))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.highFivers.size must beEqualTo(1)
      aggInfo.highFiveRecipients.size must beEqualTo(1)
    }

    "correctly track multiple people high fiving one other" in {
      val aggInfo = new HighFiveAggregatedEventInfo()

      aggInfo.addEvent(HighFiveUserEvent("Bob", DateTime.parse("2014-08-01T01:00"), "Alice"))
      aggInfo.addEvent(HighFiveUserEvent("Eve", DateTime.parse("2014-08-01T01:00"), "Alice"))
      aggInfo.addEvent(HighFiveUserEvent("James", DateTime.parse("2014-08-01T01:00"), "Alice"))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.highFivers.size must beEqualTo(3)
      aggInfo.highFiveRecipients.size must beEqualTo(1)
    }

    "correctly track one person high fiving multiple others" in {
      val aggInfo = new HighFiveAggregatedEventInfo()

      aggInfo.addEvent(HighFiveUserEvent("Bob", DateTime.parse("2014-08-01T01:00"), "Alice"))
      aggInfo.addEvent(HighFiveUserEvent("Bob", DateTime.parse("2014-08-01T01:00"), "Eve"))
      aggInfo.addEvent(HighFiveUserEvent("Bob", DateTime.parse("2014-08-01T01:00"), "James"))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.highFivers.size must beEqualTo(1)
      aggInfo.highFiveRecipients.size must beEqualTo(3)
    }

    "correctly track multiple people high fiving multiple other people (with no repeats on either end)" in {
      val aggInfo = new HighFiveAggregatedEventInfo()

      aggInfo.addEvent(HighFiveUserEvent("Bob", DateTime.parse("2014-08-01T01:00"), "Alice"))
      aggInfo.addEvent(HighFiveUserEvent("Alice", DateTime.parse("2014-08-01T01:00"), "Bob"))
      aggInfo.addEvent(HighFiveUserEvent("Eve", DateTime.parse("2014-08-01T01:00"), "James"))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.highFivers.size must beEqualTo(3)
      aggInfo.highFiveRecipients.size must beEqualTo(3)
    }

    "correctly track multiple people high fiving multiple other people (with some repeats)" in {
      val aggInfo = new HighFiveAggregatedEventInfo()

      aggInfo.addEvent(HighFiveUserEvent("Bob", DateTime.parse("2014-08-01T01:00"), "Alice"))
      aggInfo.addEvent(HighFiveUserEvent("Alice", DateTime.parse("2014-08-01T01:00"), "Bob"))
      aggInfo.addEvent(HighFiveUserEvent("Eve", DateTime.parse("2014-08-01T01:00"), "James"))
      aggInfo.addEvent(HighFiveUserEvent("Eve", DateTime.parse("2014-08-01T01:00"), "Bob"))
      aggInfo.addEvent(HighFiveUserEvent("Bob", DateTime.parse("2014-08-01T01:00"), "James"))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.highFivers.size must beEqualTo(3)
      aggInfo.highFiveRecipients.size must beEqualTo(3)
    }
  }

  "LeaveRoomAggregatedEventInfo" should {
    "correctly track one person leaving the room" in {
      val aggInfo = new LeaveRoomAggregatedEventInfo()

      aggInfo.addEvent(LeaveRoomEvent("Bob", DateTime.parse("2014-08-01T01:00")))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.people.size must beEqualTo(1)
    }

    "correctly track multiple non-repeating people leaving the room" in {
      val aggInfo = new LeaveRoomAggregatedEventInfo()

      aggInfo.addEvent(LeaveRoomEvent("Bob", DateTime.parse("2014-08-01T01:00")))
      aggInfo.addEvent(LeaveRoomEvent("Alice", DateTime.parse("2014-08-01T01:00")))
      aggInfo.addEvent(LeaveRoomEvent("Eve", DateTime.parse("2014-08-01T01:00")))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.people.size must beEqualTo(3)
    }

    "correctly track one person leaving the room multiple times" in {
      val aggInfo = new LeaveRoomAggregatedEventInfo()

      aggInfo.addEvent(LeaveRoomEvent("Bob", DateTime.parse("2014-08-01T01:00")))
      aggInfo.addEvent(LeaveRoomEvent("Bob", DateTime.parse("2014-08-01T02:00")))
      aggInfo.addEvent(LeaveRoomEvent("Bob", DateTime.parse("2014-08-01T03:00")))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.people.size must beEqualTo(1)
    }

    "correctly track multiple people repeatedly leaving the room" in {
      val aggInfo = new LeaveRoomAggregatedEventInfo()

      aggInfo.addEvent(LeaveRoomEvent("Bob", DateTime.parse("2014-08-01T01:00")))
      aggInfo.addEvent(LeaveRoomEvent("Bob", DateTime.parse("2014-08-01T02:00")))
      aggInfo.addEvent(LeaveRoomEvent("Alice", DateTime.parse("2014-08-01T01:00")))
      aggInfo.addEvent(LeaveRoomEvent("Alice", DateTime.parse("2014-08-01T02:00")))
      aggInfo.addEvent(LeaveRoomEvent("Eve", DateTime.parse("2014-08-01T01:00")))

      aggInfo.hasAnyEvents must beTrue
      aggInfo.people.size must beEqualTo(3)
    }
  }

}
